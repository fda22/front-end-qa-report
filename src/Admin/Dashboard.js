import './Dashboard.css';
import React from 'react';
import { getUser, removeUserSession } from '../Utils/Common';
import DataProject from './Project/DataProject';
import PrivateRoute from '../Utils/PrivateRoute';

// import DataProject from './Project/DataProject';
// import AddProject from './Project/AddProject';
// import EditProject from './Project/EditProject';

// import DataProjectTest from './Project/ProjectTest/DataProjectTest';
// import AddProjectTest from './Project/ProjectTest/AddProjectTest';
// import EditProjectTest from './Project/ProjectTest/EditProjectTest';

import { BrowserRouter, Switch, Route} from 'react-router-dom';
function Dashboard(props) {
  

  // handle click event of logout button
  const handleLogout = () => {
    removeUserSession();
    props.history.push('/Login');
  }
  return (
        <main className="content">
            <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
              <div className="container">
                  <div className="navbar">
                      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                          <li className="nav-item me-2">
                          <a className="nav-link" onClick={handleLogout}>
                          Logout
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div className="d-flex align-items-center ms-auto">
                      
                  </div>
              </div>
            </div>
            <DataProject/>
          </main>
  );
}

export default Dashboard;
