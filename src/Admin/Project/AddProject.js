import React, { useState }  from 'react';
import {Link} from 'react-router-dom';
import {URLProject, URLTest} from '../../ApiUrl';


import axios from 'axios';
function AddProject(props) {
    const nama = useFormInput('');
    const url = useFormInput('');
    const repo = useFormInput('');

    const handleStore = () => {
        axios.post(URLProject, { 
            nama: nama.value,
            url: url.value, 
            repo_url: repo.value 
        }).then(res => {
            props.history.push('/Dashboard');
            console.log(res);
            console.log(res.data);
          });
      }

return (

    <main>
        <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
              <div className="container">
                  <div className="navbar">
                      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                          <li className="nav-item me-2">
                          <a className="nav-link">
                          Logout
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div className="d-flex align-items-center ms-auto">
                      
                  </div>
              </div>
            </div>
        <div className="bg-primary  mw-100 mh-100">
            <div className="container">
                <div className="table-settings mb-4">
                    <p className="text-center">
                        <Link to='/Dashboard' className="text-gray-100">
                                <i className="fas fa-angle-left me-2"></i> Back to home
                        </Link>
                    </p>
                        <button className="btn btn-outline-success" onClick={handleStore}>
                            <span className="fas fa-plus me-2"></span>Add
                        </button>
                </div>
                    <div className="row">
                        <div className="col">
                            <div className="card card-body shadow-sm">
                                <h2 className="h5 mb-4">Project</h2>
                                <form>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Nama</label>
                                                <input type="text" className="form-control" {...nama} name="nama" placeholder="Nama Project"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Url</label>
                                                <input type="url" pattern="https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)(.jpg|.png|.gif)" className="form-control" {...url} name="url" placeholder="Url Project"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Repo Url</label>
                                                <input type="url" className="form-control" {...repo} name="repo" placeholder="Repo Url Project"></input>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    
)
}
const useFormInput = initialValue => {
    const [value, setValue] = useState(initialValue);
      
        const handleChange = e => {
          setValue(e.target.value);
    }
    return {
    value,
     onChange: handleChange
         }
       } 

export default AddProject;