import '../Dashboard.css';
import axios from 'axios';
import React, {useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import { getUser } from '../../Utils/Common';
import swal from 'sweetalert';
import {URLProject, URLTest} from '../../ApiUrl';



function DataProject(props) {
    // const user = getUser();
    const [projects, setprojects] = useState([]);
    const [search, setSearch] = useState('');
    const [filteredProject, setFilteredProject] = useState([]);

    

    useEffect(() => {
        axios.get(URLProject)
        .then(res => {
             const project = res.data;
             setprojects(project)
        })
        console.log(projects);
    }, [])

    useEffect(() => {
        setFilteredProject(
            projects.filter( project => {
                return project.nama.toString().toLowerCase().includes(search.toLowerCase())
            })
        )
    }, [search, projects])

    const deleteData = (id) => {
        swal({
            title: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              axios.delete(`${URLProject}${id+'/'}`, { 
                  header: {
                    'Accept':'applicaton/json',
                    'Content-Type':'applicaton/json'
                  }
                }) 
              swal("Data sukses di hapus", {
                icon: "success",
              });
            } else {
              swal("Data gagal di hapus");
            }
            window.location.reload()
          });
        
          console.log("Project dengan Id =  " + id);
    }
    
  function editData(id){
    console.log(id);
    window.location.href= "/EditProject/"+id;

    }
    function readTestData(id){
        console.log(id);
        window.location.href= "/DataTest/"+id;
    
        }
    return (

                <div className="bg-primary  mw-100 mh-100 text-white">
                    <div className="container">
                        <div className="table-settings mb-4">
                        <h3 className="h5">Admin</h3>
                        {/* <h3 className="h5">{user.name}</h3> */}
                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                            <div className="btn-toolbar">
                            <Link to='/AddProject'>
                                <button className="btn btn-outline-success">
                                    <span className="fas fa-plus me-2"></span>New Project
                                </button>
                           </Link>
                            </div>
                        </div>
                            <div className="row align-items-center justify-content-between">
                                <div className="col col-md-6 col-lg-3 col-xl-4">
                                    <div className="input-group">
                                        <span className="input-group-text" id="basic-addon2"><span className="fas fa-search"></span></span>
                                        <input type="text" onChange={e => setSearch(e.target.value)} className="form-control" id="exampleInputIconLeft" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="card border-light shadow-sm mb-4">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-centered table-nowrap mb-0 rounded">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th className="border-0">Project</th>
                                                    <th className="border-0">Url</th>
                                                    <th className="border-0">Repo Url</th>
                                                    <th className="border-0">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {filteredProject && filteredProject.map((item, i) =>
                                                <tr key={i}>
                                                    <td className="border-0 fw-bold">{item.nama}</td>
                                                    <td className="border-0">
                                                        <a className="text-primary" target="_blank" href={item.url}>{item.url}</a>
                                                    </td>
                                                    <td className="border-0">
                                                        <a className="text-primary" href={item.repo_url} target="_blank">{item.repo_url}</a>
                                                    </td>
                                                    <td className="border-0">
                                                        <i className="far fa-eye me-2" onClick={() => readTestData(item.id)}></i>

                                                        <i className="fas fa-edit me-2" onClick={() => editData(item.id)}></i>
                                                        <i className="fas fa-trash" onClick={() => deleteData(item.id)}></i> 
                                                    </td>
                                                </tr>
                                            )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    );
}

export default DataProject;
