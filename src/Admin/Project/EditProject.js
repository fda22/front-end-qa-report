import axios from 'axios';
import {Link} from 'react-router-dom';
import {URLProject, URLTest} from '../../ApiUrl';

import React, {useEffect, useState } from 'react';
function EditProject(props) {
    const [projectsTest, setprojectsTest] = useState([]);
    const [data, setData] = useState({
        nama:"",
        url:"",
        repo_url:"",
    })

    useEffect(() => {
    const id = props.match.params.id
        axios.get(URLProject+id+'/')
        .then(res => {
             const test = res.data;
             setData(test)
        })
        console.log(projectsTest);
    }, [])

   

    const handleUpdate = (e) => {
    const id = props.match.params.id
        e.preventDefault()
        axios.put(URLProject+id+'/', 
            data
        ).then(res => {
            console.log(res.data);  
            props.history.push('/Dashboard');
          });
      }
      function handleChange(e){
          const newdata ={...data}
          newdata[e.target.id]=e.target.value
          setData(newdata)

      }

return (
    <main>
        <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
              <div className="container">
                  <div className="navbar">
                      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                          <li className="nav-item me-2">
                          <a className="nav-link">
                          Logout
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div className="d-flex align-items-center ms-auto">
                      
                  </div>
              </div>
            </div>
        <div className="bg-primary  mw-100 mh-100">
            <div className="container">
                <div className="table-settings mb-4">
                    <p className="text-center">
                        <Link to='/Dashboard' className="text-gray-100">
                                <i className="fas fa-angle-left me-2"></i> Back to home
                        </Link>
                    </p>
                        <input type="submit" value="Update" className="btn btn-outline-success" onClick={(e)=>handleUpdate(e)}>
                        </input>
                </div>
                    <div className="row">
                        <div className="col">
                            <div className="card card-body shadow-sm">
                                <h2 className="h5 mb-4">Project</h2>
                                <form>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Nama</label>
                                                <input id="nama" type="text" className="form-control" onChange={(e)=> handleChange(e)} defaultValue={data.nama}  ></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Url</label>
                                                <input id="url" type="text" className="form-control" onChange={(e)=> handleChange(e)} defaultValue={data.url} ></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Repo Url</label>
                                                <input id="repo_url" type="text" className="form-control" onChange={(e)=> handleChange(e)} defaultValue={data.repo_url} ></input>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    
)
}

export default EditProject;