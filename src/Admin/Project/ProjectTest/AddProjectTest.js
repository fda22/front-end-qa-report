import {Link} from 'react-router-dom';
import axios from 'axios';
import {URLProject, URLTest} from '../../../ApiUrl';
import React, {useEffect, useState } from 'react';

function AddProjectTest(props) {
    const [projects, setprojects] = useState();

    useEffect(() => {
        axios.get(URLProject)
        .then(res => {
             const project = res.data;
             setprojects(project)
        })
        console.log(projects);
    }, [])

    // const project = useFormInput('');
    // const versi = useFormInput('');
    // const id_commit = useFormInput('');
    // const file_html = useFormInput(null);
    // const file_json = useFormInput(null);
    // const [project,setProject] = useState('');
    const [versi,setVersi] = useState('');
    const [id_commit,setIdcommit] = useState('');
    const [file_html,setHtml] = useState(null);
    const [file_json, setJson] = useState(null);

    const id = props.match.params.id
    
    const [data, setData] = useState({
        id:""
    })
    
    

    useEffect(() => {
        const id = props.match.params.id
        axios.get(URLProject+id+'/')
        .then(res => {
            const projects = res.data;
            const dataTerambil = {};
            dataTerambil.id = projects.id
            setData(dataTerambil)
            console.log(dataTerambil);
        })
        console.log(setData);
    }, [])


    function handleChange(e){
        const newdata ={...data}
        newdata[e.target.id]=e.target.value
      //   const files = e.target.files[0]
        console.log("Data:" ,newdata)
        setData(newdata)
    }


    const handleStore = (e) => {
        e.preventDefault();
        let form_data = new FormData();
        if (file_html != null) {
            form_data.append("file_html", file_html);
        }
        if (file_json != null) {
            form_data.append("file_json", file_json);
        }
        form_data.append('project', data.id);
        form_data.append('versi', versi);
        form_data.append('id_commit', id_commit);
        console.log()
        axios.post(URLTest, form_data, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            }})
            
            .then(res => {
            props.history.push('/DataTest/'+data.id);
            console.log(res);
            console.log(res.data);
          });
      }

return (

    <main>
        <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
              <div className="container">
                  <div className="navbar">
                      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                          <li className="nav-item me-2">
                          <a className="nav-link">
                          Logout
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div className="d-flex align-items-center ms-auto">
                      
                  </div>
              </div>
            </div>
<form enctype="multipart/form-data">
        <div className="bg-primary  mw-100 mh-100">
            <div className="container">
                <div className="table-settings mb-4">
                    <p className="text-center">
                        <a onClick={()=>window.history.back()} className="text-gray-100">
                                <i className="fas fa-angle-left me-2"></i> Back
                        </a>
                    </p>
                        <input type="submit" className="btn btn-outline-success" onClick={handleStore}>
                            {/* <span className="fas fa-plus me-2"></span>Add */}
                        </input>
                </div>
                    <div className="row">
                        <div className="col">
                            <div className="card card-body shadow-sm">
                                <h2 className="h5 mb-4">Project Test</h2>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Versi</label>
                                                <input className="form-control" type="text" onChange={(e)=> handleChange(e)} defaultValue={data.id} hidden></input>
                                                <input type="text" value={versi} onChange={(e)=>setVersi(e.target.value)} className="form-control" placeholder="Versi Project"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Id Commit</label>
                                                <input type="text" value={id_commit} onChange={(e)=>setIdcommit(e.target.value)} className="form-control"placeholder="Id Commit Project"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">File Html</label>
                                                <input type="file" accept='.html' id="file_html" onChange={(e)=>{setHtml(e.target.files[0])}} className="form-control"></input>
                                            </div>
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">File Json</label>
                                                <input type="file" accept='.json' id="file_json" onChange={(e)=>{setJson(e.target.files[0])}} className="form-control"></input>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
        </main>
    
)
}

// const useFormInput = initialValue => {
//     const [value, setValue] = useState(initialValue);
//     const [files, setFile] = useState(initialValue);
      
//         const handleChange = e => {
//           setValue(e.target.value);
//           setFile(e.target.files);
//     }
//     return {
//     value,files,
//      onChange: handleChange
//          }
//        } 

export default AddProjectTest;