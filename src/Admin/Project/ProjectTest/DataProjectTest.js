import '../../Dashboard.css';
import axios from 'axios';
import React, {useEffect, useState } from 'react';
import {URLProject, URLTest} from '../../../ApiUrl';
import {Link} from 'react-router-dom';
import swal from 'sweetalert';


function DataProjectTest(props) {
    const [projects, setprojects] = useState(null);
    const [projectsTest, setProjectsTest] = useState(null);

    function formatDate(string){
        var options = { year: 'numeric', day: 'numeric', month: 'long', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: 'false' };
        return new Date(string).toLocaleDateString([],options);
    }
    useEffect(() => {
    const id = props.match.params.id
        axios.get(URLProject+id+'/')
        .then(res => {
             const project = res.data;
             setprojects(project)
        })
        console.log(projects);
        axios.get(URLTest)
        .then(res => {
             const test = res.data;
             setProjectsTest(test)
        })
        console.log(projectsTest);
    }, [])


    const deleteData = (id) => {
        swal({
            title: "Apakah anda yakin akan menghapus data ini?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              axios.delete(`${URLTest}${id+'/'}`, { 
                  header: {
                    'Accept':'applicaton/json',
                    'Content-Type':'applicaton/json'
                  }
                }) 
              swal("Data sukses di hapus", {
                icon: "success",
              });
              
            } else {
              swal("Data gagal di hapus");
            }
            window.location.reload()
          });
          console.log("Project Test dengan Id =  " + id);
    }
    function EditTest(id){
        console.log(id);
        window.location.href= "/EditDataTest/"+id;
        
        }
        function AddTest(id){
            window.location.href= "/AddDataTest/"+id;
            
            }
    return (

        <main className="content">
            <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
              <div className="container">
                  <div className="navbar">
                      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                          <li className="nav-item me-2">
                          <a className="nav-link">
                          Logout
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div className="d-flex align-items-center ms-auto">
                      
                  </div>
              </div>
            </div>
                <div className="bg-primary  mw-100 mh-100 text-white">
                    <div className="container">
                        <div className="table-settings mb-4">
                        <p className="text-center">
                            <Link to='/Dashboard' className="text-gray-100">
                                <i className="fas fa-angle-left me-2"></i> Back
                            </Link>
                        </p>
                        <div>
                            <h3 className="fw-bolder">Report Testing</h3>
                                <p className="mb-0 fw-bold">Project : {projects && projects.nama}</p>
                                <p className="mb-0">Url : <a href={projects && projects.url} target="_blank">{projects && projects.url}</a></p>
                                <p className="mb-0">Repo Url : <a href={projects && projects.repo_url} target="_blank">{projects && projects.repo_url}</a></p>
                                <p className="mb-0">Created at : {projects && formatDate(projects.created_at)}</p><br/>
                        </div>
                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                            <div className="btn-toolbar">
                                <button className="btn btn-outline-success" onClick={()=>AddTest(props.match.params.id)}>
                                    <span className="fas fa-plus me-2"></span>New Test
                                </button>
                            </div>
                        </div>
                        </div>
                        <div className="row">
                            <div className="card border-light shadow-sm mb-4">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-centered table-nowrap mb-0 rounded">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th className="border-0">Versi</th>
                                                    <th className="border-0">Id Commit</th>
                                                    <th className="border-0">File Json</th>
                                                    <th className="border-0">File Html</th>
                                                    <th className="border-0">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {projectsTest && projectsTest.filter(projectsTest => projectsTest.project == props.match.params.id).map( (item, i) =>
                                                <tr item={i}>
                                                    <td className="border-0 fw-bold">{item.versi}</td>
                                                    <td className="border-0">{item.id_commit}</td>
                                                    <td className="border-0">
                                                        <a className='text-primary' href={item.file_json} target='_blank'>{ item.file_json ? item.file_json.replace(/^.*[\\\/]/, '') : null}</a>
                                                    </td>
                                                    <td className="border-0">
                                                        <a className='text-primary' href={item.file_html} target='_blank'>{item.file_html ? item.file_html.replace(/^.*[\\\/]/, '') : null}</a>
                                                    </td>
                                                    <td className="border-0">
                                                        <i className="fas fa-edit me-3" onClick={() => EditTest(item.id)}></i> 
                                                        <i className="fas fa-trash" onClick={() => deleteData(item.id)}></i> 
                                                    </td>
                                                </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </main>
    );
}

export default DataProjectTest;
