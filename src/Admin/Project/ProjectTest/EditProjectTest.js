import axios from 'axios';
import {Link} from 'react-router-dom';
import React, {useEffect, useState } from 'react';
import {URLProject, URLTest} from '../../../ApiUrl';

function EditProjectTest(props) {
    const [file_html,setHtml] = useState(null);
    const [file_json, setJson] = useState(null);
    const [data, setData] = useState({
        id:"",
        project:"",
        versi:"",
        id_commit:"",
        file_html:null,
        file_json:null,
        url_html:"",
        url_json:"",

    })
    
    

    useEffect(() => {
        const id = props.match.params.id
        axios.get(URLTest+id+'/')
        .then(res => {
            const test = res.data;
            const dataTerambil = {};
            dataTerambil.id = test.id
            dataTerambil.project = test.project
            dataTerambil.versi = test.versi
            dataTerambil.id_commit = test.id_commit
            dataTerambil.file_html = file_html
            dataTerambil.file_json = file_json
            dataTerambil.url_html = test.file_html
            dataTerambil.url_json = test.file_json
            setData(dataTerambil)
            console.log(dataTerambil);
        })
        console.log(setData);
    }, [])

      const handleUpdate = (e) => {
        const id = props.match.params.id
            e.preventDefault()
            let updatedData = new FormData()
            if (file_html != null) {
                updatedData.append("file_html", file_html);
            }
            if (file_json != null) {
                updatedData.append("file_json", file_json);
            }
            updatedData.append("id", data.id);
            updatedData.append("project", data.project);
            updatedData.append("versi" ,data.versi);
            updatedData.append("id_commit",data.id_commit);
            console.log('Test', updatedData)
            axios.put(URLTest+id+'/', updatedData,{
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json',
                }} 
            ).then(res => {
                console.log(res.data);  
                props.history.push('/DataTest/'+data.project);
              });
          }
          function handleChange(e){
              const newdata ={...data}
              newdata[e.target.id]=e.target.value
            //   const files = e.target.files[0]
              console.log("Data:" ,newdata)
              setData(newdata)
          }
          function backUrl(id) {
            console.log(id);
            window.history.back();
          }

return (
    <main>
        <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
              <div className="container">
                  <div className="navbar">
                      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                          <li className="nav-item me-2">
                          <a className="nav-link">
                          Logout
                          </a>
                          </li>
                      </ul>
                  </div>
                  <div className="d-flex align-items-center ms-auto">
                      
                  </div>
              </div>
            </div>
        <div className="bg-primary  mw-100 mh-100">
            <div className="container">
                <div className="table-settings mb-4">
                    <p className="text-center">
                                <Link onClick={()=>backUrl()} className="text-gray-100">
                                    <i className="fas fa-angle-left me-2"></i> Back
                                </Link>
                    </p>
                        <button className="btn btn-outline-success" onClick={(e)=>handleUpdate(e)}>
                            <span className="fas fa-plus me-2"></span>Update
                        </button>
                </div>
                    <div className="row">
                        <div className="col">
                            <div className="card card-body shadow-sm">
                                <h2 className="h5 mb-4">Project Test</h2>
                                <form encType="multipart/form-data">
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Versi</label>
                                                <input type="text" id="versi" onChange={(e)=> handleChange(e)} defaultValue={data.versi} className="form-control" ></input>
                                                <input type="text" id="project" onChange={(e)=> handleChange(e)} defaultValue={data.project} className="form-control" hidden></input>
                                            </div>
                                        </div>
                                        <div className="col-md-6 mb-3">
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">Id Commit</label>
                                                <input type="text" id="id_commit" onChange={(e)=> handleChange(e)} defaultValue={data.id_commit} className="form-control"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">File Html</label>
                                                <div className="small ms-2 mb-2">   Current File : <a href={data.url_html} target="_blank">{data.url_html ? data.url_html.replace(/^.*[\\\/]/, '') : null}</a></div>
                                                <input type="file"  className="form-control" onChange={(e)=>{setHtml(e.target.files[0])}}></input>

                                            </div>
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <div>
                                                <label className="form-label">File Json</label>
                                                <div className="small ms-2 mb-2">Current File : <a href={data.url_json} target="_blank">{data.url_json ? data.url_json.replace(/^.*[\\\/]/, '') : null}</a></div>
                                                <input type="file"  className="form-control" onChange={(e)=>{setJson(e.target.files[0])}}></input>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    
)
}

export default EditProjectTest;