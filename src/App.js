import './App.css';
import React, { useState, useEffect } from 'react';
import Login from './Login';
import DetailProject from './DetailProject';
import Home from './Home';
import Dashboard from './Admin/Dashboard';
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import axios from 'axios';

import DataProject from './Admin/Project/DataProject';
import AddProject from './Admin/Project/AddProject';
import EditProject from './Admin/Project/EditProject';

import DataProjectTest from './Admin/Project/ProjectTest/DataProjectTest';
import AddProjectTest from './Admin/Project/ProjectTest/AddProjectTest';
import EditProjectTest from './Admin/Project/ProjectTest/EditProjectTest';

import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import {URLCek} from './ApiUrl';

import { getToken, removeUserSession, setUserSession } from './Utils/Common';

function App() {
  const [authLoading, setAuthLoading] = useState(true);

  useEffect(() => {
    const token = getToken();
    if (!token) {
      return;
    }

    axios.get(URLCek).then(response => {
      setUserSession(response.data.token, response.data.user);
      // setAuthLoading(false);
    }).catch(error => {
      removeUserSession();
    });
  }, []);

  // if (authLoading && getToken()) {
  //   return <div className="content">Checking Authentication...</div>
  // }

  return (
    <BrowserRouter>
      <Switch>
        <PublicRoute path="/" exact component={Home}/>
        <PublicRoute path="/Project/:id" exact component={DetailProject}/>
        <PublicRoute path="/Login" component={Login} />
        <PrivateRoute path="/Dashboard" exact component={Dashboard} />

        
        <PrivateRoute path="/AddProject" component={AddProject} />
        <PrivateRoute path="/EditProject/:id" exact component={EditProject} />

        <PrivateRoute path="/DataTest/:id/" component={DataProjectTest} />
        <PrivateRoute path="/AddDataTest/:id" component={AddProjectTest} />
        <PrivateRoute path="/EditDataTest/:id" component={EditProjectTest} />

      </Switch>
    </BrowserRouter>
    
  );
}

export default App;
