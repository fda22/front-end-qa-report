import './Home.css';
import Navbar from './Navbar';
import axios from 'axios';
import {Link} from 'react-router-dom';
import React, {useEffect, useState } from 'react';
import {URLProject, URLTest} from './ApiUrl';



function DetailProject(props) {

    const id = props.match.params.id
    const [projects, setprojects] = useState();
    const [projectsTest, setProjectsTest] = useState();

    function formatDate(string){
        var options = { year: 'numeric', day: 'numeric', month: 'long', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: 'false' };
        return new Date(string).toLocaleDateString([],options);
    }
    
    useEffect(() => {
        const id = props.match.params.id
            axios.get(URLProject+id+'/')
            .then(res => {
                 const project = res.data;
                 setprojects(project)
            })
            console.log(projects);
            axios.get(URLTest)
            .then(res => {
                 const test = res.data;
                 setProjectsTest(test)
            })
            console.log(projectsTest);
        }, [])
    

    return (

        <main className="content">
            <Navbar/>
                <div className="bg-primary  mw-100 mh-100 text-white">
                    <div className="container">
                        <div className="table-settings mb-4">
                        <p className="text-center">
                            <Link to='/' className="text-gray-100">
                                <i className="fas fa-angle-left me-2"></i> Back to home
                            </Link>
                        </p>
                        <div>
                        <h3 className="fw-bolder">Report Testing</h3>
                                <p className="mb-0 fw-bold">Project : {projects && projects.nama}</p>
                                <p className="mb-0">Url : <a href={projects && projects.url} target="_blank">{projects && projects.url}</a></p>
                                <p className="mb-0">Repo Url : <a href={projects && projects.repo_url} target="_blank">{projects && projects.repo_url}</a></p>
                                <p className="mb-0">Created at : {projects && formatDate(projects.created_at)}</p><br/>
                        </div>
                            {/* <div className="row align-items-center justify-content-between">
                                <div className="col col-md-6 col-lg-3 col-xl-4">
                                    <div className="input-group">
                                        <span className="input-group-text" id="basic-addon2"><span className="fas fa-search"></span></span>
                                        <input type="text" className="form-control" id="exampleInputIconLeft" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2"></input>
                                    </div>
                                </div>
                            </div> */}
                        </div>
                        <div className="row">
                            <div className="card border-light shadow-sm mb-4">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-centered table-nowrap mb-0 rounded">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th className="border-0">Versi</th>
                                                    <th className="border-0">Id Commit</th>
                                                    <th className="border-0">File Json</th>
                                                    <th className="border-0">File Html</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {projectsTest && projectsTest.filter(projectsTest => projectsTest.project == props.match.params.id).map( (item, i) =>
                                                <tr item={i}>
                                                    <td className="border-0 fw-bold">{item.versi}</td>
                                                    <td className="border-0">{item.id_commit}</td>
                                                    <td className="border-0">
                                                        <a className='text-primary' href={item.file_json} target='_blank' download>{item.file_json.replace(/^.*[\\\/]/, '')}</a>
                                                    </td>
                                                    <td className="border-0">
                                                        <a className='text-primary' href={item.file_html} target='_blank' download={item.file_html}>{item.file_html.replace(/^.*[\\\/]/, '')}</a>
                                                    </td>
                                                </tr>
                                                )}
                                                {/* <tr>
                                                    <td className="border-0 fw-bold">2.1</td>
                                                    <td className="border-0">asdasdasd</td>
                                                    <td className="border-0">ajsndkjansjd.json</td>
                                                    <td className="border-0">ldansdknasd.html</td>
                                                </tr> */}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </main>
    );
}

export default DetailProject;
