import './Home.css';
import Navbar from './Navbar';
import axios from 'axios';
import React, {useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import {URLProject} from './ApiUrl';

    // const $ = require("jquery");
    // $.Datatable = require("datatables.net")
function Home(props) {
    const [projects, setprojects] = useState([]);
    const [search, setSearch] = useState('');
    const [filteredProject, setFilteredProject] = useState([]);

    

    useEffect(() => {
        axios.get(URLProject)
        .then(res => {
             const project = res.data;
             setprojects(project)
        })
        console.log(projects);
    }, [])

    useEffect(() => {
        setFilteredProject(
            projects.filter( project => {
                return project.nama.toString().toLowerCase().includes(search.toString().toLowerCase())
            })
        )
    }, [search, projects])

    function readData(id){
        console.log(id);
        props.history.push("/Project/"+id);
    }
    return (

    <main className="content">
            <Navbar/>
                <div className="bg-primary  mw-100 mh-100 text-white">
                    <div className="container">
                        <div className="table-settings mb-4">
                        <h3 className="fw-bolder">QA Report</h3>
                            <div className="row align-items-center justify-content-between">
                                <div className="col col-md-6 col-lg-3 col-xl-4">
                                    <div className="input-group">
                                        <span className="input-group-text" id="basic-addon2"><span className="fas fa-search"></span></span>
                                        {/* <input type="text" className="form-control"placeholder="Search" aria-label="Search" aria-describedby="basic-addon2"></input> */}
                                        <input type="text" onChange={e => setSearch(e.target.value)} className="form-control"placeholder="Search" aria-label="Search" aria-describedby="basic-addon2"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="card border-light shadow-sm mb-4">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-centered table-nowrap mb-0 rounded display" id="table_id">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th className="border-0">Nama Project</th>
                                                    <th className="border-0">Detail</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {filteredProject && filteredProject.map((item, i) =>
                                                <tr key={i}>
                                                    <td className="border-0 fw-bold">{item.nama}</td>
                                                    <td className="border-0">
                                                        <i className="fas fa-info" onClick={() => readData(item.id)}></i>
                                                    </td>
                                                </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </main>
    );
}

export default Home;
