import './Login.css';
import React, { useState } from 'react';
import axios from 'axios';
import bgLogin from './bgLogin.png';
import { setUserSession } from './Utils/Common';
import { Link } from "react-router-dom";
import {URLLogin} from './ApiUrl';



function Login(props) {
	const [loading, setLoading] = useState(false);
	const username = useFormInput('');
	const password = useFormInput('');
	const [error, setError] = useState(null);
  
	// handle button click of login form
	const handleLogin = () => {
	  setError(null);
	  setLoading(true);
	  axios.post(URLLogin, { username: username.value, password: password.value })
		.then(response => {
			setLoading(false);
			setUserSession(response.data.token, response.data.user);
			props.history.push('/Dashboard');
		}).catch((error) => {
        setLoading(false);
        if (error.response === 401)
          setError(error.response.data.message);
        else setError("Something went wrong. Please try again later.");
      });
	}
		return (
			<main className="content">
			<section className="d-flex align-items-center my-5 mt-lg-6 mb-lg-5">
				<div className="container">
					<p className="text-center">
						<Link to='/' className="text-gray-200">
							<i className="fas fa-angle-left me-2"></i> 
						Back to home</Link>
					</p>
					<div className="row justify-content-center form-bg-image" data-background-lg={bgLogin}>
						<div className="col-12 d-flex align-items-center justify-content-center">
							<div className="bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
								<div className="text-center text-md-center mb-4 mt-md-0">
									<h1 className="mb-0 h3">Login</h1>
								</div>
								<form action="#" className="mt-4">
									<div className="form-group mb-4">
										<label >Your Email</label>
										<div className="input-group">
											<span className="input-group-text" id="basic-addon1"><span className="fas fa-user"></span></span>
											<input type="text" {...username} className="form-control" placeholder="Username" id="username" ></input>
										</div>  
									</div>
									<div className="form-group">
										<div className="form-group mb-4">
											<label>Your Password</label>
											<div className="input-group">
												<span className="input-group-text" id="basic-addon2"><span className="fas fa-unlock-alt"></span></span>
												<input type="password" {...password} placeholder="Password" className="form-control" id="password"></input>
											</div>  
										</div>
									</div>
									<div className="d-grid">
									{error && (
										<>
											<small style={{ color: "red" }}>{error}</small>
											<br />
										</>
										)}
										<button type="submit" className="btn btn-dark" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading}>Login</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			</main>
	  );
  }
  const useFormInput = initialValue => {
	const [value, setValue] = useState(initialValue);
  
	const handleChange = e => {
	  setValue(e.target.value);
	}
	return {
	  value,
	  onChange: handleChange
	}
  }
  

export default Login;
