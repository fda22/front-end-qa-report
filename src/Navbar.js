import './Navbar.css';
import React from 'react';
import { Link } from "react-router-dom";

function Navbar() {
    return (
        <div id="navbar-main" aria-label="Primary navigation" className="navbar-lg navbar-theme-primary navbar-dark">
            <div className="container position">
                <div className="navbar">
                    <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
                        <li className="nav-item me-2">
                        <Link to="/Login" className="nav-link">
                         Login
                     </Link>
                        </li>
                    </ul>
                </div>
                <div className="d-flex align-items-center ms-auto">
                    
                </div>
            </div>
        </div>
      );
}

export default Navbar;
